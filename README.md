# Serilog.Sinks.File

## Requirements

- [ ] Ansible 2.14.1 or later
- [ ] Python3

Install ansible collections:

`ansible-galaxy install -r requirements.yml`

## Getting started

Prepare instance with Ubuntu Server 22.04.1

Add this instance to ansible [inventory](https://docs.ansible.com/ansible/latest/inventory_guide/intro_inventory.html)

Set env variables for jenkins username and password.

```
export JENKINS_ADMIN_USERNAME=username
export JENKINS_ADMIN_PASSWORD=userpassword
```

For prepare the build environment run playbook:

`ansible-playbook playbooks/jenkins-server.yml`

- [ ] jenkins-server role install and configure jenkins server
- [ ] jenkins-builder role install .NET Core SDK
- [ ] jfrog-cli role install jfrog cli tool for puplish artifacts

After installation login to Jenkins UI with username and password previously mentioned.
Create credentials for publishing artifacts:

```
id: jfrog-artifactory-credentials
username: yourusername
password: yourpassword
```